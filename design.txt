# COMP9201 Assignment 2 Design document

## Outline

### the related layers are:

- Application & system call layer
- Bridge layer (we need to implement most of our code in this layer)
- VFS layer

### we will use:

- per-thread fd table
- global op table



## Application & system call layer

### asst2.c

those function only has prototype (unistd.h), but has no definition, call those function will trigger system call

- write()
- open()
- read()
- close()
- lseek()
- dup2()

### assembly language

the assembly code will create a trapframe structure (trapframe.h) as following:

- store EX_SYS in trapframe.tf_cause
- store SYS_read (syscall.h) in  trapframe.tf_v0
- store the 1st argument in trapframe.tf_a0
- store the 2nd argument in trapframe.tf_a1
- store the 3rd argument in trapframe.tf_a2
- ...

### trap.c

in this function, it pass the argument tf and invoke syscall()

### syscall.c

- retreive trapframe.tf_v0 to a local varible callno
- using a switch-case statement to determine which system call is

```c
switch (callno) {
		case SYS_read:
		we need to call our Bridge layer function here
		break;
```



## Bridge layer

the bridge layer functions are largely in:

- file.h
- file.c

we name our functions:

- sys_write()
- sys_open()
- sys_read()
- sys_close()
- sys_lseek()
- sys_dup2()

### how to pass the arguments

we have trapframe in syscall.c, we can pass argument like this:

```c
int filehandle = tf->tf_a0;
void * buf = (void*)tf->tf_a1;
size_t size = (size_t)tf->tf_a2;
sys_read(filehandle, buf, size, &retval);
```

### how to generate return value

Based on [OS/161 Reference Manual - Read](http://cgi.cse.unsw.edu.au/~cs3231/18s1/os161/man/syscall/read.html), read() function should return like this:

- when success
  - The count of bytes read is returned.
- when failed
  - returns -1
  - sets errno to a suitable error code for the error condition encountered

however, the return of sys_read() function should like this:

- when success
  - return 0
  - set retval the bytes that read is returned.
- when failed
  - returns the error code defined in errno.h

this is because in syscall.c, we have

```c
	if (err) {
		/*
		 * Return the error code. This gets converted at
		 * userlevel to a return value of -1 and the error
		 * code in errno.
		 */
		tf->tf_v0 = err;
		tf->tf_a3 = 1;      /* signal an error */
	}
	else {
		/* Success. */
		tf->tf_v0 = retval;
		tf->tf_a3 = 0;      /* signal no error */
	}
```
together with the assembly code, it will convert the return of sys_read() to the return of read()



## VFS layer

this is the layer below Bridge layer, we need to use those functions in implementing our Bridge layer functions:

### vfspath.c

- vfs_open(char *path, int openflags, mode_t mode, struct vnode **ret)
- vfs_close(struct vnode *vn)

these functions will be used in implementing sys_open() and sys_close(), a copyin() function (in copyinout.c) may be needed to copy string from user space to kernel.

### vnode.h

- VOP_WRITE(vn, uio) 
- VOP_READ(vn, uio) 

these functions will be used in implementing sys_read() and sys_write(), a uio_kinit() function (in uno.c) may be needed to generate uio.

- VOP_STAT(vn, ptr)  to get the size of the file
- VOP_ISSEEKABLE(vn) 

these functions will be used in implementing sys_lseek().



## per-thread fd table

we need to implement a fd table inside thread, we will use a fixed-size 1-dimensional array:

- the size is __OPEN_MAX (limits.h)
- the index is fd
- the content is a pointer to the entry of global of table

in order to do that, we need:

- add fd table in thread.h
- initialize fd table in thread.c

we can use curthread macro to retrieve fd table from thread:

`curthread->fdtable[0]`

### stdout & stderr

based on the requirement of Assignment, 1 (stdout) and 2 (stderr) must start out attached to the console device ("con:"), we need to initialize index 1 and index 2 of fd table in runprogram.c

This initialization function may be long, so we'd like to implement it in separated files called filetable.h and filetable.c.



## global op table

the op table is set up by kmalloc(), and will be removed by kfree(), the pointer to the entry is in fd table inside thread

the content of op table may have:

- a pointer to vnode
- file pointer, namely offset of the file
- open flag (readonly, writeonly, readwrite...)
- reference count (how many time was the file opened)
- a lock for concurrency control
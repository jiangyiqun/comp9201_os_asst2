#include <types.h>
#include <kern/errno.h>
#include <kern/fcntl.h>
#include <kern/limits.h>
#include <kern/stat.h>
#include <kern/seek.h>
#include <lib.h>
#include <uio.h>
#include <thread.h>
#include <current.h>
#include <synch.h>
#include <vfs.h>
#include <vnode.h>
#include <file.h>
#include <syscall.h>
#include <copyinout.h>
#include <kern/seek.h>
#include <kern/stat.h>
#include <vnode.h>
#include <filetable.h>
#include <dup2.h>

int sys_dup2(int oldfd, int newfd, int *retval)
{
	// kprintf("DUP2\n");

	int result = 0;

	if (oldfd >= OPEN_MAX || oldfd < 0 || newfd >= OPEN_MAX || newfd < 0)
	{
		return EBADF;
	}

	if (oldfd == newfd)
	{
		*retval = newfd;
		return 0;
	}

	if (curthread->fdtable[oldfd] == NULL)
	{
		return EBADF;
	}

	if (curthread->fdtable[newfd] != NULL)
	{
		result = sys_close(newfd, retval);
		if (result)
		{
			return EBADF;
		}
	}
	else
	{
		curthread->fdtable[newfd] = (struct filedesc *)kmalloc(sizeof(struct filedesc *));
	}


	lock_acquire(curthread->fdtable[oldfd]->lk);
	
	set_fdtable(newfd, 	curthread->fdtable[oldfd]->vn, 
						curthread->fdtable[oldfd]->file_name, 
						curthread->fdtable[oldfd]->flags, 
						curthread->fdtable[oldfd]->offset, 1);

	lock_release(curthread->fdtable[oldfd]->lk);
	*retval = newfd;
	// kprintf("DUP2 END\n");
	return 0;
}
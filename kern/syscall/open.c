#include <types.h>
#include <kern/errno.h>
#include <kern/fcntl.h>
#include <kern/limits.h>
#include <kern/stat.h>
#include <kern/seek.h>
#include <lib.h>
#include <uio.h>
#include <thread.h>
#include <current.h>
#include <synch.h>
#include <vfs.h>
#include <vnode.h>
#include <file.h>
#include <syscall.h>
#include <copyinout.h>
#include <kern/seek.h>
#include <kern/stat.h>
#include <vnode.h>
#include <filetable.h>
#include <open.h>

int sys_open(const char *filename, int flags, mode_t mode, int *retval)
{
	// kprintf("OPEN\n");

	int result = 0, index = 3;
	struct vnode *vn;
	char *kbuf;
	size_t len;

	kbuf = (char *)kmalloc(sizeof(char) * PATH_MAX);
	result = copyinstr((const_userptr_t)filename, kbuf, PATH_MAX, &len);
	if (result)
	{
		kprintf("OPEN --- copyinstr failed- %d\n", result);
		kfree(kbuf);
		return result;
	}

	// Find Mini num of unused filedesc in curthread table
	while (curthread->fdtable[index] != NULL)
	{
		index++;
	}

	// Max number check
	if (index >= OPEN_MAX)
	{
		kprintf("OPEN --- Max open file\n");
		kfree(kbuf);
		return ENFILE;
	}

	curthread->fdtable[index] = (struct filedesc *)kmalloc(sizeof(struct filedesc *));
	result = vfs_open(kbuf, flags, mode, &vn);
	if (result)
	{
		kprintf("OPEN --- vfs open failed\n");
		kfree(kbuf);
		kfree(curthread->fdtable[index]);
		curthread->fdtable[index] = NULL;
		return result;
	}

	set_fdtable(index, vn, kbuf, flags, 0, 1);

	*retval = index;
	kfree(kbuf);
	// kprintf("OPEN END\n");
	return 0;
}
#include <types.h>
#include <kern/errno.h>
#include <kern/fcntl.h>
#include <kern/limits.h>
#include <kern/stat.h>
#include <kern/seek.h>
#include <lib.h>
#include <uio.h>
#include <thread.h>
#include <current.h>
#include <synch.h>
#include <vfs.h>
#include <vnode.h>
#include <file.h>
#include <syscall.h>
#include <copyinout.h>
#include <kern/seek.h>
#include <kern/stat.h>
#include <vnode.h>
#include <filetable.h>

off_t sys_lseek(int filehandle, off_t pos, int whence, int *retval, int *retval_lseek)
{
	// kprintf("LSEEK\n");

	int result;
	
	result = check_fh_ft(filehandle);
	if (result) {
		return EBADF;
	}

	off_t offset, file_size;
	struct stat statbuf;

	lock_acquire(curthread->fdtable[filehandle]->lk);
	result = VOP_STAT(curthread->fdtable[filehandle]->vn, &statbuf);
	if (result)
	{
		lock_release(curthread->fdtable[filehandle]->lk);
		return result;
	}

	file_size = statbuf.st_size;

	if (whence == SEEK_SET)
	{
		result = VOP_ISSEEKABLE(curthread->fdtable[filehandle]->vn);
		if (!result)
		{
			lock_release(curthread->fdtable[filehandle]->lk);
			return result;
		}
		offset = pos;
	}
	else if (whence == SEEK_CUR)
	{
		result = VOP_ISSEEKABLE(curthread->fdtable[filehandle]->vn);
		if (!result)
		{
			lock_release(curthread->fdtable[filehandle]->lk);
			return result;
		}
		offset = curthread->fdtable[filehandle]->offset + pos;
	}
	else if (whence == SEEK_END)
	{
		result = VOP_ISSEEKABLE(curthread->fdtable[filehandle]->vn);
		if (!result)
		{
			lock_release(curthread->fdtable[filehandle]->lk);
			return result;
		}
		offset = file_size + pos;
	}
	else
	{
		lock_release(curthread->fdtable[filehandle]->lk);
		return EINVAL;
	}

	if (offset < (off_t)0)
	{
		lock_release(curthread->fdtable[filehandle]->lk);
		return EINVAL;
	}
	curthread->fdtable[filehandle]->offset = offset;

	*retval = (uint32_t)((offset & 0xFFFFFFFF00000000) >> 32);
	*retval_lseek = (uint32_t)(offset & 0xFFFFFFFF);

	lock_release(curthread->fdtable[filehandle]->lk);

	// kprintf("LSEEK END\n");
	return 0;
}
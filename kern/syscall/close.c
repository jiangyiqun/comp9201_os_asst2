#include <types.h>
#include <kern/errno.h>
#include <kern/fcntl.h>
#include <kern/limits.h>
#include <kern/stat.h>
#include <kern/seek.h>
#include <lib.h>
#include <uio.h>
#include <thread.h>
#include <current.h>
#include <synch.h>
#include <vfs.h>
#include <vnode.h>
#include <file.h>
#include <syscall.h>
#include <copyinout.h>
#include <kern/seek.h>
#include <kern/stat.h>
#include <vnode.h>
#include <filetable.h>
#include <close.h>

int sys_close(int filehandle, int *retval)
{
	// kprintf("CLOSE\n");
	int result;

	result = check_fh_ft(filehandle);
	if (result) {
		return EBADF;
	}

	if (curthread->fdtable[filehandle]->vn == NULL)
	{
		return EBADF;
	}

	if (curthread->fdtable[filehandle]->refcount == 1)
	{
		vfs_close(curthread->fdtable[filehandle]->vn);
		lock_destroy(curthread->fdtable[filehandle]->lk);
		kfree(curthread->fdtable[filehandle]);
		curthread->fdtable[filehandle] = NULL;
	} 
	else
	{
		curthread->fdtable[filehandle]->refcount -= 1;
	}
	*retval = 0;
	// kprintf("CLOSE  END\n");
	return 0;
}
#include <types.h>
#include <kern/errno.h>
#include <kern/fcntl.h>
#include <kern/limits.h>
#include <kern/stat.h>
#include <kern/seek.h>
#include <lib.h>
#include <uio.h>
#include <thread.h>
#include <current.h>
#include <synch.h>
#include <vfs.h>
#include <vnode.h>
#include <file.h>
#include <syscall.h>
#include <copyinout.h>
#include <kern/seek.h>
#include <kern/stat.h>
#include <vnode.h>
#include <filetable.h>
#include <write.h>

int sys_write(int filehandle, const void *buf, size_t size, int *retval)
{
	// kprintf("WRITE");

	int result = 0;

	result = check_fh_ft(filehandle);
	if (result) {
		return EBADF;
	}

	if (curthread->fdtable[filehandle]->flags == O_RDONLY)
	{
		return EBADF;
	}

	struct iovec iov;
	struct uio ku;
	void *kbuf;
	kbuf = kmalloc(sizeof(*buf) * size);
	if (kbuf == NULL)
	{
		return EINVAL;
	}

	lock_acquire(curthread->fdtable[filehandle]->lk);

	result = copyin((const_userptr_t)buf, kbuf, size);
	if (result)
	{
		kfree(kbuf);
		lock_release(curthread->fdtable[filehandle]->lk);
		return result;
	}

	uio_kinit(&iov, &ku, kbuf, size, curthread->fdtable[filehandle]->offset, UIO_WRITE);

	result = VOP_WRITE(curthread->fdtable[filehandle]->vn, &ku);

	if (result)
	{
		kfree(kbuf);
		lock_release(curthread->fdtable[filehandle]->lk);
		return result;
	}

	curthread->fdtable[filehandle]->offset = ku.uio_offset;

	*retval = size - ku.uio_resid;
	kfree(kbuf);
	lock_release(curthread->fdtable[filehandle]->lk);
	// kprintf("WRITE  END");
	return 0;
}
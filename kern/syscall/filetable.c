
#include <types.h>
#include <kern/fcntl.h>
#include <kern/errno.h>
#include <lib.h>
#include <uio.h>
#include <thread.h>
#include <current.h>
#include <synch.h>
#include <vfs.h>
#include <vnode.h>
#include <syscall.h>
#include <copyinout.h>
#include <kern/seek.h>
#include <kern/stat.h>
#include <vnode.h>
#include <filetable.h>

int filetable_init(void)
{
	struct vnode *vn;
	char *con = NULL;
	int result;

	// STDIN
	con = kstrdup("con:");
	result = vfs_open(con, O_RDONLY, 0, &vn);
	if (result){
		kprintf("INIT- IN Initialization Failed\n");
		kfree(con);
		vfs_close(vn);
		return EINVAL;
	}

	set_fdtable(0, vn, con, O_RDONLY, 0, 1);

	// STDOUT
	con = kstrdup("con:");
	result = vfs_open(con, O_WRONLY, 0, &vn);
	if (result){
		kprintf("INIT- OUT Initialization Failed\n");
		kfree(con);
		vfs_close(vn);
		lock_destroy(curthread->fdtable[0]->lk);
		kfree(curthread->fdtable[0]);
		return EINVAL;
	}

	set_fdtable(1, vn, con, O_WRONLY, 0, 1);

	// STDERR
	con = kstrdup("con:");
	result = vfs_open(con, O_WRONLY, 0, &vn);
	if (result){
		kprintf("INIT- ERR Initialization Failed\n");
		kfree(con);
		vfs_close(vn);
		lock_destroy(curthread->fdtable[0]->lk);
		kfree(curthread->fdtable[0]);
		lock_destroy(curthread->fdtable[1]->lk);
		kfree(curthread->fdtable[1]);
		return EINVAL;
	}

	set_fdtable(2, vn, con, O_WRONLY, 0, 1);

	return 0;
}

void set_fdtable(int num, struct vnode *vn, char *con, int flag, off_t  offset, int refcount)
{
	curthread->fdtable[num] = (struct filedesc *)kmalloc(sizeof(struct filedesc));
	curthread->fdtable[num]->vn = vn;
	strcpy(curthread->fdtable[num]->file_name, con);
	curthread->fdtable[num]->flags = flag;
	curthread->fdtable[num]->offset = offset;
	curthread->fdtable[num]->refcount = refcount;
	curthread->fdtable[num]->lk = lock_create(con);
}

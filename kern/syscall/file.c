#include <types.h>
#include <kern/errno.h>
#include <kern/fcntl.h>
#include <kern/limits.h>
#include <kern/stat.h>
#include <kern/seek.h>
#include <lib.h>
#include <uio.h>
#include <thread.h>
#include <current.h>
#include <synch.h>
#include <vfs.h>
#include <vnode.h>
#include <file.h>
#include <syscall.h>
#include <copyinout.h>
#include <kern/seek.h>
#include <kern/stat.h>
#include <vnode.h>
#include <filetable.h>

int check_fh_ft(int filehandle)
{
	if (filehandle >= OPEN_MAX || filehandle < 0)
	{
		return 1;
	}

	if (curthread->fdtable[filehandle] == NULL)
	{
		return 2;
	}

	return 0;
}
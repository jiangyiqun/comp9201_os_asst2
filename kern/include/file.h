/*
 * Declarations for file handle and file table management.
 */

#ifndef _FILE_H_
#define _FILE_H_

/*
 * Contains some file-related maximum length constants
 */

#include <read.h>
#include <write.h>
#include <close.h>
#include <open.h>
#include <dup2.h>
#include <lseek.h>

int check_fh_ft(int filehandle);

#endif /* _FILE_H_ */

#ifndef _CLOSE_H_
#define _CLOSE_H_

int sys_close(int filehandle, int *retval);

#endif
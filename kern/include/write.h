#ifndef _WRITE_H_
#define _WRITE_H_

#include <types.h>

int sys_write(int filehandle, const void *buf, size_t size, int *retval);

#endif
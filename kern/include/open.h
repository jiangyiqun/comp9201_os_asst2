
#ifndef _OPEN_H_
#define _OPEN_H_

int sys_open(const char *filename, int flags,mode_t mode, int *retval);

#endif
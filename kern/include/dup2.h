
#ifndef _DUP2_H_
#define _DUP2_H_

int sys_dup2(int oldfd, int newfd, int *retval);

#endif
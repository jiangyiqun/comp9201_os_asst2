#ifndef _READ_H_
#define _READ_H_

#include <types.h>

int sys_read(int filehandle, void *buf, size_t size, int *retval);

#endif
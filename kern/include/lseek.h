

#ifndef _LSEEK_H_
#define _LSEEK_H_

off_t sys_lseek(int filehandle, off_t pos, int whence, int *retval, int *retval_lseek);

#endif
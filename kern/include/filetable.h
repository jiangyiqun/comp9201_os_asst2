
#ifndef _FILETABLE_H_
#define _FILETABLE_H_

#include <lib.h>
#include <array.h>
#include <synch.h>
#include <types.h>
#include <vnode.h>
#include <limits.h>

struct filedesc{
	char file_name[__NAME_MAX];	//	File name
	struct vnode *vn; 	//	Reference to the underlying file 'object'
	off_t offset;     	//	Offset into the file
	struct lock *lk;   	//	Synchronization
	int flags;      	//	Flags with which the file was opened
	int refcount; 		//	Reference count
};

int filetable_init(void);
void set_fdtable(int num, struct vnode *vn, char *con, int flag, off_t  offset, int refcount);

#endif
